from django.db import models

# Create your models here.
global image_url
image_url = 'image/'


class Dish(models.Model):
    name = models.CharField(max_length = 20,default = '默认')
    price = models.FloatField()
    material = models.TextField(max_length = 200)
    cost = models.FloatField()
    image = models.FileField(upload_to = image_url)


class User(models.Model):
    account = models.CharField(max_length=20)
    password = models.CharField(max_length=20)
    name = models.CharField(max_length=20)
    salary = models.FloatField()
    Role_CHOICE=(
        ('manager','经理'),
        ('host','迎宾'),
        ('waiter','服务员'),
        ('busboy','清洁工'),
        ('cook','厨师')
    )
    role = models.CharField(choices=Role_CHOICE, max_length=20)


class Table(models.Model):
    STATE_CHOICE=(
        ('free','空闲'),
        ('settled','已落坐'),
        ('ordered','已点单'),
        ('left','已离开')
    )
    state = models.CharField(choices=STATE_CHOICE, max_length=20)
    number = models.IntegerField()
    left = models.IntegerField()
    top = models.IntegerField()


class OrderInfo(models.Model):
    date = models.DateTimeField(auto_now_add=True)
    table = models.ForeignKey(Table, on_delete=models.CASCADE)
    waiter = models.ForeignKey(User, on_delete=models.CASCADE)
    total_price=models.IntegerField()
    note = models.CharField(max_length=100,default = '默认')


class Cuisine(models.Model):
    cook = models.ForeignKey(User,on_delete=models.CASCADE)
    dish = models.ForeignKey(Dish,on_delete=models.CASCADE)
    order = models.ForeignKey(OrderInfo,on_delete=models.CASCADE)
    STATE_CHOICE=(
        ('unaccept','未接取'),
        ('unfinished','完成中'),
        ('finished','已完成'),
    )
    state = models.CharField(choices=STATE_CHOICE, max_length=20)

class CuisineLog(models.Model):
    date = models.DateTimeField(auto_now_add=True)
    cuisine = models.ForeignKey(Cuisine, on_delete=models.CASCADE)
    STATE_CHOICE=(
        ('unfinished','完成中'),
        ('finished','已完成'),
    )
    target = models.CharField(choices=STATE_CHOICE, max_length=20)
    cook = models.ForeignKey(User, on_delete=models.CASCADE)

class TableLog(models.Model):
    date = models.DateTimeField(auto_now_add=True)
    table = models.ForeignKey(Table, on_delete=models.CASCADE)
    STATE_CHOICE=(
        ('free','空闲'),
        ('settled','已落坐'),
        ('ordered','已点单'),
        ('left','已离开')
    )
    target = models.CharField(choices=STATE_CHOICE, max_length=20)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    
