from django.urls import path, include
from . import views


urlpatterns = [
    path('createDish/', views.create_dish),
    path('signIn/', views.sign_in),
    path('getAllDishes/', views.get_all_dishes),
    path('createUser/', views.create_user),
    path('getAllTables/',views.get_all_tables),
    path('createTable/',views.create_table),
    path('getAllUsers/', views.get_all_users),
    path('changeState/',views.change_table),
    path('getAllOrders/',views.get_all_orders),
    path('getOrderCuisines/',views.get_order_cuisines),
    path('createOrder/',views.create_order),
    path('getAllCuisine/',views.get_all_cuisines),
    path('getMyCuisines/',views.get_my_cuisines),
    path('changeTaskState/',views.change_task_state),
    path('deleteDish/',views.delete_dish),
    path('getDishesIncome/',views.get_dishes_income),
    path('changeUser/',views.change_user),
    path('changeDish/',views.change_dish),
    path('getDishesIncome/',views.get_dishes_income),
    path('deleteUser/',views.delete_user),
    path('getOrderProcess/',views.get_order_process),
    path('changeTablePosition/',views.change_table_position)
]
