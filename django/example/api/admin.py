from django.contrib import admin

from .models import Dish, User, Table, OrderInfo, Cuisine, CuisineLog, TableLog


# Register your models here.
admin.site.register(Dish)
admin.site.register(User)
admin.site.register(Table)
admin.site.register(OrderInfo)
admin.site.register(Cuisine)
admin.site.register(CuisineLog)
admin.site.register(TableLog)
