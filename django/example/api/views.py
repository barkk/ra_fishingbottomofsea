from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
from .models import CuisineLog, Dish, TableLog, User, Table, OrderInfo, Cuisine
from django.utils import timezone
from datetime import datetime


# Create your views here.
def create_dish(request):
    dish_name = request.POST.get('name')
    dish_price = request.POST.get('price')
    dish_material = request.POST.get('material')
    dish_cost = request.POST.get('cost')
    dish_image = request.FILES.get('image')
    Dish.objects.create(name=dish_name, price=dish_price,
                        material=dish_material, cost=dish_cost, image=dish_image)
    return HttpResponse('success')


def delete_dish(request):
    dishes_id = request.POST.get('dishes_id')
    dishToDelete = Dish.objects.get(id=dishes_id)
    if dishToDelete != None:
        dishToDelete.delete()
        return HttpResponse('success')
    else:
        return HttpResponse('failure')


def sign_in(request):
    user_account = request.POST.get('account')
    user_password = request.POST.get('password')
    if User.objects.filter(account=user_account, password=user_password).exclude(name='无').exists():
        return JsonResponse(
            list(User.objects.filter(account=user_account,
                 password=user_password).values('id', 'role')),
            json_dumps_params={'ensure_ascii': False}, safe=False)
    else:
        return HttpResponse('fail')


def get_all_dishes(request):
    dishes = Dish.objects.all().values(
        'id', 'name', 'price', 'cost', 'image', 'material')
    dishes_list = list(dishes)
    for l in dishes_list:
        l['count'] = 0
        l['sum'] = 0
    return JsonResponse(dishes_list, json_dumps_params={'ensure_ascii': False}, safe=False)


def create_user(request):
    user_account = request.POST.get('account')
    user_password = request.POST.get('password')
    user_name = request.POST.get('name')
    user_salary = request.POST.get('salary')
    user_role = request.POST.get('role')
    User.objects.create(account=user_account, password=user_password,
                        name=user_name, salary=user_salary, role=user_role)
    return HttpResponse('success')


def delete_user(request):
    users_id = request.POST.get('users_id')
    userToDelete = User.objects.get(id=users_id)
    if userToDelete != None:
        userToDelete.delete()
        return HttpResponse('success')
    else:
        return HttpResponse('failure')


def get_all_tables(request):
    tables = Table.objects.all().values('id', 'number', 'state','left','top')
    tables_list = list(tables)
    for table in tables_list:
        table['showManual']=False
        if table['state'] == 'free':
            table['state'] = '空闲' 
            table['color']='#d2d2d2'    # 灰
        elif table['state'] == 'settled':
            table['state'] = '已落坐'
            table['color']='#67c23a'    # 绿
        elif table['state'] == 'ordered':
            table['state'] = '已点菜'
            table['color']='#e6a23c'    # 棕
        elif table['state'] == 'left':
            table['state'] = '已离开'
            table['color']='#f56c6c'    # 红
    return JsonResponse(tables_list, json_dumps_params={'ensure_ascii': False}, safe=False)


def create_table(request):
    table_number = request.POST.get('number')
    table_state = request.POST.get('state')
    table_left=request.POST.get('left')
    table_right=request.POST.get('top')
    Table.objects.create(number=table_number, state=table_state,left=table_left,right=table_right)
    return HttpResponse('success')


def change_table(request):
    user_id = request.POST.get('userId')
    table_number = request.POST.get('number')
    table_oldstate = request.POST.get('oldstate')
    table_newstate = request.POST.get('newstate')
    table = Table.objects.get(number=table_number)
    user = User.objects.get(id=user_id)
    if table_oldstate == '空闲' and table_newstate == 'settled':
        table.state = 'settled'
        table.save()
    if table_oldstate == '已点菜' and table_newstate == 'left':
        table.state = 'left'
        table.save()
    if table_oldstate == '已离开' and table_newstate == 'free':
        table.state = 'free'
        table.save()
    TableLog.objects.create(table=table, target=table_newstate, user=user)
    return HttpResponse('success')


def get_all_users(request):
    users = User.objects.all().exclude(name='无').values('id', 'name', 'salary', 'role')
    users_list = list(users)
    for user in users_list:
        if user['role'] == 'cook':
            cook = User.objects.get(id=user['id'])
            user['productivity'] = CuisineLog.objects.filter(
                cook=cook, target='finished', date__gt=timezone.now().date()).count()
        elif user['role'] == 'waiter':
            waiter = User.objects.get(id=user['id'])
            user['productivity'] = TableLog.objects.filter(
                user=waiter, target='ordered', date__gt=timezone.now().date()).count()
        elif user['role'] == 'busboy':
            busboy = User.objects.get(id=user['id'])
            user['productivity'] = TableLog.objects.filter(
                user=busboy, target='free', date__gt=timezone.now().date()).count()
        else:
            user['productivity'] = '尚未统计'
    return JsonResponse(users_list, json_dumps_params={'ensure_ascii': False}, safe=False)


def get_all_orders(request):
    orders = OrderInfo.objects.all().values(
        'id', 'date', 'waiter', 'table', 'total_price', 'note')
    orders_list = list(orders)
    for order in orders_list:
        order['date'] = order['date'].strftime("%Y-%m-%d")
        order['table'] = Table.objects.get(id=order['table']).number
        order['waiter'] = User.objects.get(id=order['waiter']).name
    return JsonResponse(orders_list, json_dumps_params={'ensure_ascii': False}, safe=False)


def get_order_cuisines(request):
    order_id = request.GET.get('id')
    this_order = OrderInfo.objects.get(id=order_id)
    cuisines = Cuisine.objects.filter(
        order=this_order).values('id', 'cook', 'dish', 'state')
    cuisines_list = list(cuisines)
    head_of_unaccept = this_order.date
    for cuisine in cuisines_list:
        cuisine['cook'] = User.objects.get(id=cuisine['cook']).name
        cuisine['dish'] = Dish.objects.get(id=cuisine['dish']).name
        cuisine['headOfUnaccept'] = head_of_unaccept.strftime('%Y-%m-%d %H:%M:%S')
        cuisine['headOfUnfinished'] = '无'
        cuisine['headOfFinished'] = '无'
        this_cuisine = Cuisine.objects.get(id=cuisine['id'])
        if cuisine['state'] == 'unaccept':
            cuisine['state'] = '未接取'
        elif cuisine['state'] == 'unfinished':
            cuisine['state'] = '完成中'
            cuisine_log = CuisineLog.objects.get(
                cuisine=this_cuisine, target='unfinished')
            cuisine['headOfUnfinished'] = cuisine_log.date.strftime('%Y-%m-%d %H:%M:%S')
        else:
            cuisine_log = CuisineLog.objects.get(
                cuisine=this_cuisine, target='unfinished')
            cuisine['headOfUnfinished'] = cuisine_log.date.strftime('%Y-%m-%d %H:%M:%S')
            cuisine_log = CuisineLog.objects.get(
                cuisine=this_cuisine, target='finished')
            cuisine['headOfFinished'] = cuisine_log.date.strftime('%Y-%m-%d %H:%M:%S')
            cuisine['state'] = '已完成'
    return JsonResponse(cuisines_list, json_dumps_params={'ensure_ascii': False}, safe=False)


def get_order_process(request):
    order_id = request.GET.get('id')
    order = OrderInfo.objects.get(id=order_id)
    process_list = [{}, {}, {}, {}]
    if TableLog.objects.filter(table=order.table,date__gte=order.date,target='ordered').exists():
        ordered_date = TableLog.objects.filter(table=order.table,date__gte=order.date,target='ordered')[0].date
        process_list[0]['state'] = '点菜'
        process_list[0]['time'] = ordered_date.strftime('%Y-%m-%d %H:%M:%S')
    cuisines = Cuisine.objects.filter(order=order)
    cuisine_max_time = ordered_date
    for cuisine in cuisines:
        if CuisineLog.objects.filter(cuisine=cuisine,target='finished'):
            if cuisine_max_time < CuisineLog.objects.get(cuisine=cuisine,target='finished').date:
                cuisine_max_time = CuisineLog.objects.get(cuisine=cuisine,target='finished').date
        else:
            return JsonResponse(process_list, json_dumps_params={'ensure_ascii': False}, safe=False)
    process_list[1]['state'] = '菜上齐'
    process_list[1]['time'] = cuisine_max_time.strftime('%Y-%m-%d %H:%M:%S')
    if TableLog.objects.filter(table=order.table,date__gte=ordered_date,target='left').exists():
        left_date = TableLog.objects.filter(table=order.table,date__gte=ordered_date,target='left')[0].date
        process_list[2]['state'] = '离开'
        process_list[2]['time'] = left_date.strftime('%Y-%m-%d %H:%M:%S')
    if TableLog.objects.filter(table=order.table,date__gte=ordered_date,target='free').exists():
        free_date = TableLog.objects.filter(table=order.table,date__gte=left_date,target='free')[0].date
        process_list[3]['state'] = '清洁完毕'
        process_list[3]['time'] = free_date.strftime('%Y-%m-%d %H:%M:%S')
    return JsonResponse(process_list, json_dumps_params={'ensure_ascii': False}, safe=False)


def create_order(request):
    table_id = request.POST.get('tableId')
    user_id = request.POST.get('userId')
    total_price = request.POST.get('totalPrice')
    note = request.POST.get('note')
    dishIdes = request.POST.getlist('dishId')
    table = Table.objects.get(id=table_id)
    waiter = User.objects.get(id=user_id)
    order = OrderInfo.objects.create(table=table, waiter=waiter,
                                     total_price=total_price, note=note)
    if not User.objects.filter(role='cook', name='无').exists():
        User.objects.create(account='@@@@@@', password='@@@@@@',
                            salary=0, role='cook', name='无')
    cook = User.objects.get(role='cook', name='无')
    for dishId in dishIdes:
        dish = Dish.objects.get(id=dishId)
        Cuisine.objects.create(cook=cook, dish=dish,
                               order=order, state='unaccept')
    table.state = 'ordered'
    table.save()
    TableLog.objects.create(table=table, target='ordered', user=waiter)
    return HttpResponse('success')


def get_all_cuisines(request):
    cuisines = Cuisine.objects.all().values('cook', 'dish', 'order', 'state')
    cuisine_list = list(cuisines)
    return JsonResponse(cuisine_list, json_dumps_params={'ensure_ascii': False}, safe=False)


def get_my_cuisines(request):
    cook_id = request.GET.get('id')
    cook = User.objects.get(id=cook_id)
    cuisines = Cuisine.objects.filter(cook=cook).exclude(state='finished').values(
        'id', 'dish', 'order', 'state')
    cuisines_list = list(cuisines)
    if not User.objects.filter(role='cook', name='无').exists():
        User.objects.create(account='@@@@@@', password='@@@@@@',
                            salary=0, role='cook', name='无')
    cook = User.objects.get(role='cook', name='无')
    cuisines = Cuisine.objects.filter(cook=cook).exclude(state='finished').values(
        'id', 'dish', 'order', 'state')
    cuisines_list = cuisines_list + list(cuisines)
    for cuisine in cuisines_list:
        cuisine['dish'] = Dish.objects.get(id=cuisine['dish']).name
        cuisine['order'] = OrderInfo.objects.get(id=cuisine['order']).table
        cuisine['order'] = cuisine['order'].number
        if cuisine['state'] == 'unaccept':
            cuisine['state'] = '未接取'
        elif cuisine['state'] == 'unfinished':
            cuisine['state'] = '完成中'
        else:
            cuisine['state'] = '已完成'
    return JsonResponse(cuisines_list, json_dumps_params={'ensure_ascii': False}, safe=False)


def change_task_state(request):
    cuisine_id = request.GET.get('cuisineId')
    cook_id = request.GET.get('cookId')
    cook = User.objects.get(id=cook_id)
    cuisine = Cuisine.objects.get(id=cuisine_id)
    if cuisine.state == 'unaccept':
        cuisine.state = 'unfinished'
        cuisine.cook = cook
        CuisineLog.objects.create(
            cuisine=cuisine, cook=cook, target='unfinished')
    elif cuisine.state == 'unfinished':
        cuisine.state = 'finished'
        CuisineLog.objects.create(
            cuisine=cuisine, cook=cook, target='finished')
    cuisine.save()
    return HttpResponse('success')


def get_dishes_income(request):
    cuisines = list(Cuisine.objects.all())
    dishes = list(Dish.objects.all())
    dish_map = {}
    for dish in dishes:
        temp_dic = {}
        temp_dic['name'] = dish.name
        temp_dic['income'] = dish.price-dish.cost
        temp_dic['num'] = 0
        temp_dic['finished_num'] = 0
        temp_dic['all_time_interval'] = 0
        dish_map[dish.name] = temp_dic

    all_incomes = 0
    for cui in cuisines:
        dish = cui.dish.name
        dish_map[dish]['num'] += 1
        all_incomes += dish_map[dish]['income']
        cuisinelog = list(CuisineLog.objects.filter(cuisine=cui))
        if(len(cuisinelog) == 2):
            dish_map[dish]['finished_num'] += 1
            time_interval = (cuisinelog[1].date-cuisinelog[0].date).seconds
            dish_map[dish]['all_time_interval'] += time_interval

    dish_list = []
    for key in dish_map:
        incomes = dish_map[key]['income']*dish_map[key]['num']
        dish_map[key]['allincomes'] = incomes
        dish_map[key]["percentage"] = incomes/all_incomes
        if dish_map[key]['finished_num'] == 0:
            dish_map[key]['time_interval'] = 0;
        else:
            dish_map[key]['time_interval'] = dish_map[key]['all_time_interval'] / \
            dish_map[key]['finished_num']
        dish_list.append(dish_map[key])
    return JsonResponse(dish_list, json_dumps_params={'ensure_ascii': False}, safe=False)


def change_user(request):
    userId = request.POST.get('userid')
    userName = request.POST.get('name')
    userSalary = request.POST.get('salary')
    userRole = request.POST.get('role')

    id = int(userId)
    targetUser = User.objects.get(id=id)
    temp = type(targetUser).__name__
    if targetUser != None:
        targetUser.name = userName
        targetUser.salary = userSalary
        targetUser.role = userRole
        targetUser.save()
    return JsonResponse("success", json_dumps_params={'ensure_ascii': False}, safe=False)


def change_dish(request):
    dishId = request.POST.get('id')
    dishName = request.POST.get('name')
    dishPrice = request.POST.get('price')
    dishCost = request.POST.get('cost')
    dishMaterial = request.POST.get('material')
    dishImage = request.FILES.get('image')
    targetDish = Dish.objects.get(id=dishId)

    if targetDish != None:
        targetDish.name = dishName
        targetDish.price = dishPrice
        targetDish.cost = dishCost
        targetDish.material = dishMaterial
        targetDish.image = dishImage
        targetDish.save()
    return JsonResponse("success", json_dumps_params={'ensure_ascii': False}, safe=False)


def change_table_position(request):
    tableId=request.POST.get('id')
    tableLeft=request.POST.get('left')
    tableTop=request.POST.get('top')
    tableId=int(tableId)
    tableLeft=int(tableLeft)
    tableTop=int(tableTop)
    
    myTable=Table.objects.get(id=tableId)
    myTable.left=tableLeft
    myTable.top=tableTop

    myTable.save()
    
    return JsonResponse('success', json_dumps_params={'ensure_ascii': False}, safe=False)
    