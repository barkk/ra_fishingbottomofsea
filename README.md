# FivewithZero

# Example Environment for Django, MariaDB, NginX, Gunicorn using Docker 

> Languages: [English](./README.md) | [中文](./doc/README_zh_CN.md)
>
> 项目文档：https://nankai.feishu.cn/drive/folder/fldcnH4G5e57A8ybfWDTLuaRI6d

*Please use it with caution.*

*If an error should occur, there would be something wrong with the environment.*

*You are right!*
*Believe yourself!*


## Introduction

This is a Docker setup for a web application based on Django.

- The Django application is served by Gunicorn (WSGI application).
- We use NginX as reverse proxy and static files server. Static files are persistently stored in volumes.
- MariaDB is used. Data are persistently stored in local files.
- Python dependencies are managed through pipenv, with Pipfile and Pipfile.lock.
- Tests are run using tox, pytest, and other tools such as safety, bandit, isort and prospector.

Also a Makefile is available for convenience. You might need to use `sudo make` instead of just `make` because docker and docker-compose commands often needs admin privilege.

**Note: `Make` command may not run on Windows !**

## Requirement
You need to install [Docker](https://www.docker.com/) and [Docker-Compose](https://docs.docker.com/compose/).

## Quick Start

In the first time, you should execute these commands step by step.

### Backend

**Note: You should change [example] in the below commands with your project's name.**

``` shell
    docker-compose build
    docker-compose run --rm django python example/manage.py migrate
    docker-compose run --rm django python example/manage.py collectstatic --no-input
    docker-compose down
    docker-compose up
```

*The second command may produce an error, please wait for a minute and run this command for several times.*

The server will run on port `8000`, so you can visit [localhost/backend/](localhost/backend/) to access to your Django server.

Stop the Django server with:

```shell
    docker-compose down
```

### Frontend

Please use `npm install` at frontend folder `./frontend` to install requirements before start frontend.

#### In **DEV (develop environment)**

``` shell
    npm install
    cd frontend
    npm run serve
```

The frontend will serve on [localhost/backend/](localhost/backend/)

#### In **PROD (production environment)**

``` shell
    cd frontend
    npm run build
```

The frontend webpage will be packed into `./frontend/dist/`. 

Use `docker-compose up` start NginX.
The static files will serve on NginX server, visit [localhost](localhost) to check it!

In production mode, frontend and backend are in same domain. **No need to worry about CSRF & CORS !!!**

## Commands

### Build

`docker-compose build` or `make build`

### Migrate

`docker-compose run --rm django example/manage.py migrate` or `make migrate`

### Collect static files

`docker-compose run --rm django example/manage.py collectstatic --no-input` or `make collectstatic`

### Run

`docker-compose up` or `make run`

### Create new app

`docker-compose run --rm django /bin/bash -c "cd example && python manage.py startapp <you app name>"`

### Any other commands (in Django)

If you want to execute any other commands:

`docker-compose run --rm django /bin/bash -c "<You command>"`

### Tests
- `make test`
- `make checksafety`
- `make checkstyle`
- `make coverage`
- Use `make check` to check both safety and style

### Tips

Use `docker-compose up -d` to run server on background.

## More

When you want to use your own project instead of the example project **(Not just change the name of the folder! Use command `django-admin startproject [ProjectName]` to create a new project)**, follow these steps:

1. Replace **example** with **your project file name** in file: `docker-compose.yml`, `Makefile`, `Dockerfile`, `tox.ini`, `/django/config/gunicorn/conf.py`
2. Edit the `settings.py` file in your project: 
   
    Add this line to **settings.py** after `import os`
    ``` python
        _env = os.environ
    ```
    
    Then configure the database part with:
    ``` python
        DATABASES = {
            'default': {
                'ENGINE': 'django.db.backends.mysql',
                'USER': _env['DB_USER'], 
                'PASSWORD': _env['DB_PASS'],
                'NAME': _env['DB_NAME'],
                'HOST': 'database',
                'PORT': _env['DB_PORT'], 
                'OPTIONS': {
                    # 存储引擎启用严格模式，非法数据值被拒绝
                    'init_command': "SET sql_mode='STRICT_TRANS_TABLES'",
                    'charset': 'utf8mb4',
                },
            }
        }
    
    ```

    Also a script prefix should be add in the end of `settings.py`
    ``` python
        USE_X_FORWARDED_HOST = True
        FORCE_SCRIPT_NAME = _env['URL_PREFIX']
    ```
3. Rebuild the image.
4. Use `docker-compose up` to start your project.

### Configure your DB & NginX

You can edit some basic configurations in `/config/db/db_env` for MariaDB Container and `/config/django/django_env` for Django Container.

In production environment, you should configure the port reflects in `docker-compose.yml`['services']['nginx']['ports'].

### Install other package with pip

`docker-compose run django pipenv install [package]`

**Note:** Only this container(django) contains the corresponding package.

It is also avaliable to build your own image with specify pip packages. Otherwise, every time you start the container, you need to install the relevant package.

1. Delete the existing image. 
   
    `docker-compose run --rm django /bin/bash -c "pipenv install [package] && pipenv lock"`

    `docker rmi djangoenv_django`

2. Rebuild the python(Django) image.

    `docker-compose build`

All in all, 3 steps to build your own image with the packages all you need:

``` shell
    docker-compose run --rm django /bin/bash -c "pipenv install [package] && pipenv lock"
    docker rmi djangoenv_django
    docker-compose build
```


## How it works

![structure-img](./doc/img/structure.png)

## Reference

- [使用docker-compose部署nginx+gunicorn+mariadb的django应用](https://www.lagou.com/lgeduarticle/52228.html)
- [Docker for Django(Nginx and MySQL)](https://medium.com/@kenkono/docker-for-django-nginx-and-mysql-5960a611829e)
- [Dockerizing Django with Postgres, Gunicorn, and Nginx](https://testdriven.io/blog/dockerizing-django-with-postgres-gunicorn-and-nginx/)
- [Docker Compose with NginX, Django, Gunicorn and multiple Postgres databases](https://pawamoy.github.io/posts/docker-compose-django-postgres-nginx/)

