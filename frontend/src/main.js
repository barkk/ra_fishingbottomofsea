import Vue from 'vue';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import App from './App.vue';
import router from './router';
import axios from 'axios';
import global_ from './Global.vue';
import echarts from 'echarts'

Vue.use(ElementUI);

Vue.config.productionTip = false;

axios.defaults.withCredentials = true
axios.defaults.xsrfHeaderName = 'X-CSRFTOKEN'
axios.defaults.xsrfCookieName = 'csrftoken'

Vue.prototype.GLOBAL = global_

Vue.prototype.$echarts = echarts

Vue.prototype.$http = axios.create({
    //baseURL: 'http://localhost/backend',
    baseURL: 'http://82.157.138.204/backend',
    xsrfCookieName: 'csrftoken',
    xsrfHeaderName: 'X-CSRFToken',
    timeout: 30000
})

new Vue({
    router,
    data: function() {
        return {
            role: 'guest'
        }
    },
    render: h => h(App)
}).$mount('#app')
