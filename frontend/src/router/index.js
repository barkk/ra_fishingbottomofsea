import Vue from 'vue'
import VueRouter from 'vue-router'
import DishManage from '../components/DishManage.vue'
import Order from '../components/Order.vue'
import Floor from '../components/Floor.vue'
import Kitchen from '../components/Kitchen.vue'
import PersonnelManagement from '../components/PersonnelManagement.vue'
import OrderManage from '../components/OrderManage.vue'
import OverviewOrder from '../components/OverviewOrder'
import DishAnalysis from '../components/DishAnalysis'
import OverviewDish from '../components/OverviewDish'
import DishManagement from '../components/DishManagement'

Vue.use(VueRouter)

const routes = [// eslint-disable-line no-unused-vars

	{
		path: '/DishManage',
		name: 'DishManage',
		component: DishManage
	},
	{
		path: '/Order',
		name: 'Order',
		component: Order
	},
	{
		path: '/PersonnelManagement',
		name: 'PersonnelManagement',
		component: PersonnelManagement
	},
	{
		path: '/DishManagement',
		name: 'DishManagement',
		component: DishManagement
	},
	{
		path: '/OrderManage',
		name: 'OrderManage',
		component: OrderManage
	},
	{
		path: '/Floor',
		name: 'Floor',
		component: Floor

	},

	{
		path: '/Kitchen',
		name: 'Kitchen',
		component: Kitchen

	},
	{
		path: '/OverviewOrder',
		name: 'OverviewOrder',
		component: OverviewOrder

	},
	{
		path: '/AnalysisDish',
		name: 'AnalysisDish',
		component: DishAnalysis

	},
	{
		path: '/OverviewDish',
		name: 'OverviewDish',
		component: OverviewDish

	}

]

const router = new VueRouter({
	routes
})

export default router
