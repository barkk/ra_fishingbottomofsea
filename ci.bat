start cmd /C frontend.bat
docker-compose build
docker-compose run --rm django python example/manage.py collectstatic --no-input
docker-compose run --rm django python example/manage.py makemigrations
docker-compose run --rm django python example/manage.py migrate
start cmd /C docker-compose run --rm django python example/manage.py createsuperuser
start cmd /C docker-compose up
cd frontend
npm run serve